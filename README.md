# 2015 Street Tree Census Tree Data EDA

## Как посмотреть исследование

Перейдите по ссылке [EDA Notebook](./EDA.ipynb) и сможете обнакомиться с ноутбуком.

## Где взять исходные данные

В репозитории не представлен исходный набор данных. Вы можете скачать его с сайта [Kaggle](https://www.kaggle.com/datasets/new-york-city/ny-2015-street-tree-census-tree-data/data). Вам потребуется положить файл 2015-street-tree-census-tree-data.csv в папку data.